from Utility import *
from GAN import *

###############################################################################


class Model(GANModelDesc):
	# def build_losses(self, logits_real, logits_fake, name="GAN_loss"):
	# 	with tf.name_scope(name=name):
	# 		score_real = tf.sigmoid(logits_real)
	# 		score_fake = tf.sigmoid(logits_fake)
	# 		tf.summary.histogram('score-real', score_real)
	# 		tf.summary.histogram('score-fake', score_fake)
	# 		with tf.name_scope("discrim"):
	# 			d_loss_pos = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(
	# 				logits=logits_real, labels=tf.ones_like(logits_real)), name='loss_real')
	# 			d_loss_neg = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(
	# 				logits=logits_fake, labels=tf.zeros_like(logits_fake)), name='loss_fake')

	# 			d_pos_acc = tf.reduce_mean(tf.cast(score_real > 0.5, tf.float32), name='accuracy_real')
	# 			d_neg_acc = tf.reduce_mean(tf.cast(score_fake < 0.5, tf.float32), name='accuracy_fake')

	# 			d_accuracy = tf.add(.5 * d_pos_acc, .5 * d_neg_acc, name='accuracy')
	# 			d_loss = tf.add(.5 * d_loss_pos, .5 * d_loss_neg, name='loss')
	# 		with tf.name_scope("gen"):
	# 			g_loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(
	# 				logits=logits_fake, labels=tf.ones_like(logits_fake)), name='loss')
	# 			g_accuracy = tf.reduce_mean(tf.cast(score_fake > 0.5, tf.float32), name='accuracy')
	# 			return g_loss, d_loss

	def build_losses(self, vecpos, vecneg, name="WGAN_loss"):
		with tf.name_scope(name=name):
			# the Wasserstein-GAN losses
			d_loss = tf.reduce_mean(vecneg - vecpos, name='d_loss')
			g_loss = tf.negative(tf.reduce_mean(vecneg), name='g_loss')
			# add_moving_summary(self.d_loss, self.g_loss)
			return g_loss, d_loss

	# def build_losses(self, real, fake, name="LSGAN_loss"):
	# 	d_real = tf.reduce_mean(tf.squared_difference(real, 1), name='d_real')
	# 	d_fake = tf.reduce_mean(tf.square(fake), name='d_fake')
	# 	d_loss = tf.multiply(d_real + d_fake, 0.5, name='d_loss')
	# 	tf.summary.histogram('score-real', d_real)
	# 	tf.summary.histogram('score-fake', d_fake)
	# 	g_loss = tf.reduce_mean(tf.squared_difference(fake, 1), name='g_loss')
	# 	# add_moving_summary(g_loss, d_loss)
	# 	return g_loss, d_loss

	@auto_reuse_variable_scope
	def encoder(self, image, nb_filters=24):
		assert image is not None
		return vgg19_encoder_flat(image, nb_filters=nb_filters)

	@auto_reuse_variable_scope
	def decoder(self, image, feats, nb_filters=24):
		assert image is not None
		return vgg19_decoder_flat(image, feats, nb_filters=nb_filters)


	def inputs(self):
		return [
			tf.placeholder(tf.float32, (DIMZ, DIMY, DIMX, 1), 'image'),
			tf.placeholder(tf.float32, (DIMZ, DIMY, DIMX, 1), 'label'),
			tf.placeholder(tf.float32, (DIMZ, DIMY, DIMX, 1), 'noise'),
			]

	def build_graph(self, image, label, noise):
		print(image)
		print(label)
		print(noise)
		pi, pl, pn = image, label, noise


		with tf.variable_scope('pre'): # Preprocessing
			with varreplace.freeze_variables():
				# Scale everything in between 0 and 1
				pi = pi / 255.0 # noise ppc
				pl = pl / 255.0 # clean wfly
				pn = pn / 255.0 # noise true

				# # Multiply the noise to label (wfly clean) to make input (wfly_noise)
				# pnl = pn * pl
				# pni = pi
		with tf.variable_scope('gen'):
			with tf.variable_scope('add'):
				with tf.device('/device:GPU:0'):
					with tf.variable_scope('enc0'):
						pln, feats_pln = self.encoder(tf_2tanh(tf.concat([pn, pl], axis=-1), maxVal=1.0))
					with tf.variable_scope('dec0'):
						pln, _ 		   = self.decoder(pln, feats_pln)
						pln 		   = tf_2imag(pln, maxVal=1.0)  #fake noised_ppc 	noised_w, fly
					pin 		   = tf.identity(pi)	#true noised_ppc

			with tf.variable_scope('ext'): # wfly
				with tf.device('/device:GPU:1'):
					with tf.variable_scope('enc0'):
						e1, feats_e1 = self.encoder(tf_2tanh(pln, maxVal=1.0))
					with tf.variable_scope('dec0'):
						n1, _ 		 = self.decoder(e1, [None, None, None, None, None])
						n1 		   	 = tf_2imag(n1, maxVal=1.0) 
			
					with tf.variable_scope('enc0'):
						e2, feats_e2 = self.encoder(tf_2tanh(pin, maxVal=1.0))
					with tf.variable_scope('dec0'):
						n2, _ 		 = self.decoder(e2, [None, None, None, None, None])
						n2 		   	 = tf_2imag(n2, maxVal=1.0) 	

			with tf.variable_scope('sub'):
				with tf.device('/device:GPU:2'):
					with tf.variable_scope('enc0'):
						plnn, feats_plnn = self.encoder(tf_2tanh(tf.concat([n1, pln], axis=-1), maxVal=1.0))
					with tf.variable_scope('dec0'):
						plnn, _ 	   = self.decoder(plnn, feats_plnn)
						plnn 		   = tf_2imag(plnn, maxVal=1.0) 

					with tf.variable_scope('enc0'):
						pinn, feats_pinn = self.encoder(tf_2tanh(tf.concat([n2, pin], axis=-1), maxVal=1.0))
					with tf.variable_scope('dec0'):
						pinn, _ 	   = self.decoder(pinn, feats_pinn)
						pinn 		   = tf_2imag(pinn, maxVal=1.0)  
			
			with tf.variable_scope('add'):
				with tf.device('/device:GPU:0'):
					with tf.variable_scope('enc0'):
						pinnn, feats_pinnn = self.encoder(tf_2tanh(tf.concat([n2, pinn], axis=-1), maxVal=1.0))
					with tf.variable_scope('dec0'):
						pinnn, _ 		   = self.decoder(pinnn, feats_pinnn)
						pinnn 		   = tf_2imag(pinnn, maxVal=1.0) 
					plnnn 		   = tf.identity(plnn)	

		with tf.variable_scope('discrim'):
			with tf.variable_scope('add'):
				with tf.device('/device:GPU:0'):
					real_image_0, _ = self.encoder(tf_2tanh(pi, maxVal=1.0)) # Real noised_ppc
					fake_image_1, _ = self.encoder(tf_2tanh(pln, maxVal=1.0)) # Fake noised_wfly1
					fake_image_2, _ = self.encoder(tf_2tanh(pinnn, maxVal=1.0)) # Fake noised_cyc_ppc

			with tf.variable_scope('ext'):
				with tf.device('/device:GPU:1'):
					real_noise_0, _ = self.encoder(tf_2tanh(pn, maxVal=1.0))
					fake_noise_1, _ = self.encoder(tf_2tanh(n1, maxVal=1.0))
					fake_noise_2, _ = self.encoder(tf_2tanh(n2, maxVal=1.0))

			with tf.variable_scope('sub'):
				with tf.device('/device:GPU:2'):
					real_label_0, _ = self.encoder(tf_2tanh(pl, maxVal=1.0)) # Real clean_wfly1
					fake_label_1, _ = self.encoder(tf_2tanh(plnn, maxVal=1.0))
					fake_label_2, _ = self.encoder(tf_2tanh(pinn, maxVal=1.0))
		######################################
		# Loss computation
		g_losses = []
		d_losses = []

		# MSE loss
		with tf.name_scope('loss_mae'):
			mae_noise = tf.reduce_mean(tf.abs(pn - n1), name='mae_noise')
			g_losses.append(1e2*mae_noise)
			add_moving_summary(mae_noise)

			mae_label = tf.reduce_mean(tf.abs(pl - plnn), name='mae_label')
			g_losses.append(1e2*mae_label)
			add_moving_summary(mae_label)

			mae_image = tf.reduce_mean(tf.abs(pi - pinnn), name='mae_image')
			g_losses.append(1e2*mae_image)
			add_moving_summary(mae_image)	

		with tf.name_scope('loss_gan'):
			G_noise_1, D_noise_1 = self.build_losses(real_noise_0, fake_noise_1, name='noise_1')
			g_losses.append(1e0*G_noise_1)
			d_losses.append(1e0*D_noise_1)
			
			G_noise_2, D_noise_2 = self.build_losses(real_noise_0, fake_noise_2, name='noise_2')
			g_losses.append(1e0*G_noise_2)
			d_losses.append(1e0*D_noise_2)

			#
			G_image_1, D_image_1 = self.build_losses(real_image_0, fake_image_1, name='image_1')
			g_losses.append(1e0*G_image_1)
			d_losses.append(1e0*D_image_1)
			
			G_image_2, D_image_2 = self.build_losses(real_image_0, fake_image_2, name='image_2')
			g_losses.append(1e0*G_image_2)
			d_losses.append(1e0*D_image_2)

			#
			G_label_1, D_label_1 = self.build_losses(real_label_0, fake_label_1, name='label_1')
			g_losses.append(1e0*G_label_1)
			d_losses.append(1e0*D_label_1)
			
			G_label_2, D_label_2 = self.build_losses(real_label_0, fake_label_2, name='label_2')
			g_losses.append(1e0*G_label_2)
			d_losses.append(1e0*D_label_2)

		self.g_loss = tf.reduce_mean(g_losses, name='self.g_loss')
		self.d_loss = tf.reduce_mean(d_losses, name='self.d_loss')
		self.collect_variables() # Overload function to WGAN, see above for more details

		add_moving_summary(self.d_loss, self.g_loss)
		######################################
		# Visualization
		pz 	= tf.zeros_like(pi)
		viz = tf.concat([
						 tf.concat([pl, pn, pln, n1, plnn, pz   ], axis=2), # ppc_denoised, noise_ppc, wfly_denoise, noise_fly
						 tf.concat([pz, pz, pin, n2, pinn, pinnn], axis=2), # ppc_denoised, noise_ppc, wfly_denoise, noise_fly
						 ], axis=1)
		viz = 255*(viz)
		viz = tf.cast(tf.clip_by_value(viz, 0, 255), tf.uint8, name='viz')
		tf.summary.image('colorized', viz, max_outputs=50)


	def optimizer(self):
		lr = symbolic_functions.get_scalar_var('learning_rate', 2e-4, summary=True)
		return tf.train.AdamOptimizer(lr, beta1=0.5, epsilon=1e-3)

###############################################################################
class VisualizeRunner(Callback):
	def __init__(self, input, tower_name='InferenceTower', device=0):
		self.dset = input 
		self._tower_name = tower_name
		self._device = device

	def _setup_graph(self):
		self.pred = self.trainer.get_predictor(
			['image', 'label', 'noise'], ['viz'])

	def _before_train(self):
		pass

	def _trigger(self):
		for lst in self.dset.get_data():
			viz_test = self.pred(lst)
			viz_test = np.squeeze(np.array(viz_test))

			#print viz_test.shape

			self.trainer.monitors.put_image('viz_test', viz_test)
###############################################################################
def get_data(dataDir, isTrain=False, isValid=False, isTest=False):
	# Process the directories 
	if isTrain:
		num=500
	if isValid:
		num=1
	if isTest:
		num=10

	
	dset  = ImageDataFlow(os.path.join(dataDir, 'image'),
						  os.path.join(dataDir, 'label'),
						  os.path.join(dataDir, 'noise'),
						  num, 
						  isTrain=isTrain, 
						  isValid=isValid, 
						  isTest =isTest)
	dset.reset_state()
	return dset

###############################################################################
if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('--gpu',        default='0', help='comma seperated list of GPU(s) to use.')
	parser.add_argument('--data',  default='data/wfly1/db_train/', required=True, 
									help='Data directory, contain trainA/trainB/validA/validB')
	parser.add_argument('--load',   help='Load the model path')
	parser.add_argument('--sample', help='Run the deployment on an instance',
									action='store_true')

	args = parser.parse_args()
	# python Exp_FusionNet2D_-VectorField.py --gpu='0' --data='arranged/'

	
	train_ds = get_data(args.data, 
						isTrain=True, 
						isValid=False, 
						isTest=False)
	valid_ds = get_data(args.data.replace('train', 'valid'),
						isTrain=False, 
						isValid=True, 
						isTest=False)


	train_ds  = PrefetchDataZMQ(train_ds, 4)
	train_ds  = PrintData(train_ds)
	# train_ds  = QueueInput(train_ds)
	model     = Model()

	os.environ['PYTHONWARNINGS'] = 'ignore'

	# Set the GPU
	if args.gpu:
		os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu

	# Running train or deploy
	if args.sample:
		# TODO
		# sample
		pass
	else:
		# Set up configuration
		# Set the logger directory
		logger.auto_set_dir()

		session_init = SaverRestore(args.load) if args.load else None 


		GANTrainer(StagingInput(QueueInput(train_ds)), model).train_with_defaults(
			callbacks       =   [
				PeriodicTrigger(ModelSaver(), every_k_epochs=50),
				PeriodicTrigger(VisualizeRunner(valid_ds), every_k_epochs=5),
				PeriodicTrigger(InferenceRunner(valid_ds, [ScalarStats('loss_mae/mae_noise'), 
														   ScalarStats('loss_mae/mae_image'), 
														   ScalarStats('loss_mae/mae_label'), ]), every_k_epochs=1),
				# ScheduledHyperParamSetter('learning_rate', [(0, 1e-6), (300, 1e-6)], interp='linear')
				ScheduledHyperParamSetter('learning_rate', [(0, 2e-4), (100, 1e-4), (200, 1e-5), (300, 1e-6)], interp='linear'),
				# ScheduledHyperParamSetter('learning_rate', [(30, 6e-6), (45, 1e-6), (60, 8e-7)]),
				# HumanHyperParamSetter('learning_rate'),
				ClipCallback(),
				],
				max_epoch       =   500, 
				session_init	=	session_init,
				steps_per_epoch =	EPOCH_SIZE,
		)
