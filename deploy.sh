# python SharedGenerator.py --gpu=0 --sample --load='train_log/SharedGenerator/model-50000.index'  --data='result'
# python SharedEncoder.py --gpu=0 --sample --load='train_log/SharedEncoder/model-50000.index'  --data='result'
# python SharedDecoder.py --gpu=0 --sample --load='train_log/SharedDecoder/model-50000.index'  --data='result'
# python SplitGenerator.py --gpu=0 --sample --load='train_log/SplitGenerator/model-50000.index'  --data='result'
# python SharedGenerator_Phase1.py --gpu=0 --sample --load='train_log/SharedGenerator_Phase1/model-50000.index'  --data='result'
# python SharedGenerator_Phase2.py --gpu=0 --sample --load='train_log/SharedGenerator_Phase2/model-300000.index'  --data='result'
# python SharedGenerator_Phase3.py --gpu=0 --sample --load='train_log/SharedGenerator_Phase3/model-300000.index'  --data='result'


# python SharedGeneratorPreprocess.py --gpu=0 --sample --load='train_log/SharedGeneratorPreprocess/model-50000.index'  --data='data/wfly1/db_test3/'
# python CyclicSharedGenerator.py --gpu=0 --sample --load='train_log/CyclicSharedGenerator/model-50000.index'  --data='data/wfly1/db_test3/'
# python CyclicSharedGeneratorConstraint.py --gpu=0 --sample --load='train_log/CyclicSharedGeneratorConstraint/model-50000.index'  --data='data/wfly1/db_test3/'


python SharedGenerator.py --gpu=0 --sample --load='train_log/SharedGenerator/model-250000.index'  --data='data/wfly1/db_test3/'
python SharedGenerator_Phase1.py --gpu=0 --sample --load='train_log/SharedGenerator_Phase1/model-250000.index'  --data='data/wfly1/db_test3/'
python SharedGenerator_Phase2.py --gpu=0 --sample --load='train_log/SharedGenerator_Phase2/model-500000.index'  --data='data/wfly1/db_test3/'

