# Hidden 2 domains no constrained
#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os, sys, argparse, glob, time, six

# from __future__ import absolute_import
# from __future__ import division
# from __future__ import print_function


# Misc. libraries
from six.moves import map, zip, range
from natsort import natsorted 

# Array and image processing toolboxes
import numpy as np 
import skimage
import skimage.io
import skimage.transform
import skimage.segmentation
import malis

import PIL
from PIL import Image
# Tensorpack toolbox
import tensorpack.tfutils.symbolic_functions as symbf

from tensorpack import *
from tensorpack.dataflow import dataset
from tensorpack.utils.gpu import get_nr_gpu
from tensorpack.utils.utils import get_rng
from tensorpack.tfutils import optimizer, gradproc
from tensorpack.tfutils.summary import add_moving_summary, add_param_summary, add_tensor_summary
from tensorpack.tfutils.scope_utils import auto_reuse_variable_scope
from tensorpack.utils import logger

# Tensorflow 
import tensorflow as tf

# Tensorlayer
from tensorlayer.cost import binary_cross_entropy, absolute_difference_error, dice_coe

# Sklearn
from sklearn.metrics.cluster import adjusted_rand_score

# Augmentor
import Augmentor
###############################################################################
EPOCH_SIZE = 500
NB_FILTERS = 32   # channel size

DIMX  = 640
DIMY  = 640
DIMZ  = 1
DIMC  = 1


###############################################################################
def INReLU(x, name=None):
    x = InstanceNorm('inorm', x)
    return tf.nn.relu(x, name=name)


def INLReLU(x, name=None):
    x = InstanceNorm('inorm', x)
    return tf.nn.leaky_relu(x, name=name)
    
def BNLReLU(x, name=None):
    x = BatchNorm('bn', x)
    return tf.nn.leaky_relu(x, name=name)

###############################################################################
# Utility function for scaling 
def tf_2tanh(x, maxVal = 255.0, name='ToRangeTanh'):
    with tf.variable_scope(name):

        return (x / maxVal - 0.5) * 2.0
        # x = tf.divide(x, tf.convert_to_tensor(maxVal))
        # x = tf.subtract(x, tf.convert_to_tensor(0.5))
        # x = tf.multiply(x, tf.convert_to_tensor(2.0))
        # return x
###############################################################################
def tf_2imag(x, maxVal = 255.0, name='ToRangeImag'):
    with tf.variable_scope(name):

        return (x / 2.0 + 0.5) * maxVal
        # x = tf.divide(x, tf.convert_to_tensor(2.0))
        # x = tf.add(x, tf.convert_to_tensor(0.5))
        # x = tf.multiply(x, tf.convert_to_tensor(maxVal))
        # return x

# Utility function for scaling 
def np_2tanh(x, maxVal = 255.0, name='ToRangeTanh'):
    return (x / maxVal - 0.5) * 2.0
###############################################################################
def np_2imag(x, maxVal = 255.0, name='ToRangeImag'):
    return (x / 2.0 + 0.5) * maxVal

###############################################################################
# FusionNet
@layer_register(log_shape=True)
def residual(x, chan, first=False, kernel_shape=3):
    with argscope([Conv2D], nl=INLReLU, stride=1, kernel_shape=kernel_shape):
        inputs = x
        # x = tf.pad(x, name='pad1', mode='REFLECT', paddings=[[0,0], [1*(kernel_shape//2),1*(kernel_shape//2)], [1*(kernel_shape//2),1*(kernel_shape//2)], [0,0]])
        # x = Conv2D('conv1', x, chan, padding='VALID', dilation_rate=1)
        # x = tf.pad(x, name='pad2', mode='REFLECT', paddings=[[0,0], [2*(kernel_shape//2),2*(kernel_shape//2)], [2*(kernel_shape//2),2*(kernel_shape//2)], [0,0]])
        # x = Conv2D('conv2', x, chan, padding='VALID', dilation_rate=2)
        # x = tf.pad(x, name='pad3', mode='REFLECT', paddings=[[0,0], [4*(kernel_shape//2),4*(kernel_shape//2)], [4*(kernel_shape//2),4*(kernel_shape//2)], [0,0]])
        # x = Conv2D('conv3', x, chan, padding='VALID', dilation_rate=4)             
        # x = tf.pad(x, name='pad4', mode='REFLECT', paddings=[[0,0], [8*(kernel_shape//2),8*(kernel_shape//2)], [8*(kernel_shape//2),8*(kernel_shape//2)], [0,0]])
        # x = Conv2D('conv4', x, chan, padding='VALID', dilation_rate=8)
        # x = tf.pad(x, name='pad0', mode='REFLECT', paddings=[[0,0], [kernel_shape//2,kernel_shape//2], [kernel_shape//2,kernel_shape//2], [0,0]])
        # x = Conv2D('conv0', x, chan, padding='VALID', nl=tf.identity)
        x = tf.pad(x, name='pad1', mode='REFLECT', paddings=[[0,0], [1*(kernel_shape//2),1*(kernel_shape//2)], [1*(kernel_shape//2),1*(kernel_shape//2)], [0,0]])
        x = Conv2D('conv1', x, chan, padding='VALID', dilation_rate=1)
        x = tf.pad(x, name='pad2', mode='REFLECT', paddings=[[0,0], [2*(kernel_shape//2),2*(kernel_shape//2)], [2*(kernel_shape//2),2*(kernel_shape//2)], [0,0]])
        x = Conv2D('conv2', x, chan, padding='VALID', dilation_rate=2)
        x = tf.pad(x, name='pad3', mode='REFLECT', paddings=[[0,0], [4*(kernel_shape//2),4*(kernel_shape//2)], [4*(kernel_shape//2),4*(kernel_shape//2)], [0,0]])
        x = Conv2D('conv3', x, chan, padding='VALID', dilation_rate=4)             
        # x = tf.pad(x, name='pad4', mode='REFLECT', paddings=[[0,0], [8*(kernel_shape//2),8*(kernel_shape//2)], [8*(kernel_shape//2),8*(kernel_shape//2)], [0,0]])
        # x = Conv2D('conv4', x, chan, padding='VALID', dilation_rate=8) 
        x = InstanceNorm('inorm', x) + inputs
        return x

###############################################################################
@layer_register(log_shape=True)
def residual_enc(x, chan, first=False, kernel_shape=3):
    with argscope([Conv2D, Deconv2D], nl=INLReLU, stride=1, kernel_shape=kernel_shape):
        
        x = tf.pad(x, name='pad_i', mode='REFLECT', paddings=[[0,0], [kernel_shape//2,kernel_shape//2], [kernel_shape//2,kernel_shape//2], [0,0]])
        x = Conv2D('conv_i', x, chan, stride=2) 
        x = residual('res_', x, chan, first=True)
        x = tf.pad(x, name='pad_o', mode='REFLECT', paddings=[[0,0], [kernel_shape//2,kernel_shape//2], [kernel_shape//2,kernel_shape//2], [0,0]])
        x = Conv2D('conv_o', x, chan, stride=1) 

        return x

###############################################################################
@layer_register(log_shape=True)
def residual_dec(x, chan, first=False, kernel_shape=3):
    with argscope([Conv2D, Deconv2D], nl=INLReLU, stride=1, kernel_shape=kernel_shape):
        x = Deconv2D('deconv_i', x, chan, stride=1) 
        x = residual('res2_', x, chan, first=True)
        x = Deconv2D('deconv_o', x, chan, stride=2) 

        return x

###############################################################################
@auto_reuse_variable_scope
def arch_fusionnet_2d(img, last_dim=1, nl=INLReLU, nb_filters=32):
    assert img is not None
    with argscope([Conv2D], nl=INLReLU, kernel_shape=3, stride=2, padding='VALID'):
        with argscope([Deconv2D], nl=INLReLU, kernel_shape=3, stride=2, padding='SAME'):
            e0 = residual_enc('e0', img, nb_filters*1)
            e1 = residual_enc('e1',  e0, nb_filters*2)
            e2 = residual_enc('e2',  e1, nb_filters*4)

            e3 = residual_enc('e3',  e2, nb_filters*8)
            # e3 = Dropout('dr', e3, 0.5)

            d3 = residual_dec('d3',    e3, nb_filters*4)
            d2 = residual_dec('d2', d3+e2, nb_filters*2)
            d1 = residual_dec('d1', d2+e1, nb_filters*1)
            d0 = residual_dec('d0', d1+e0, nb_filters*1) 
            dp = tf.pad( d0, name='pad_o', mode='REFLECT', paddings=[[0,0], [3//2,3//2], [3//2,3//2], [0,0]])
            dd = Conv2D('convlast', dp, last_dim, kernel_shape=3, stride=1, padding='VALID', nl=nl, use_bias=True) 
            return dd


@auto_reuse_variable_scope
def arch_fusionnet_encoder_2d(img, feats=[None, None, None], last_dim=1, nl=INLReLU, nb_filters=32):
    assert img is not None
    with argscope([Conv2D], nl=INLReLU, kernel_shape=3, stride=2, padding='VALID'):
        with argscope([Deconv2D], nl=INLReLU, kernel_shape=3, stride=2, padding='SAME'):
            e0 = residual_enc('e0', img, nb_filters*1)
            e1 = residual_enc('e1',  e0, nb_filters*2)
            e2 = residual_enc('e2',  e1, nb_filters*4)

            e3 = residual_enc('e3',  e2, nb_filters*8)
            # e3 = Dropout('dr', e3, 0.5)
            return e3, [e2, e1, e0]
            # d3 = residual_dec('d3',    e3, nb_filters*4)
            # d2 = residual_dec('d2', d3+e2, nb_filters*2)
            # d1 = residual_dec('d1', d2+e1, nb_filters*1)
            # d0 = residual_dec('d0', d1+e0, nb_filters*1) 
            # dp = tf.pad( d0, name='pad_o', mode='REFLECT', paddings=[[0,0], [3//2,3//2], [3//2,3//2], [0,0]])
            # dd = Conv2D('convlast', dp, last_dim, kernel_shape=3, stride=1, padding='VALID', nl=nl, use_bias=True) 
            # return dd

@auto_reuse_variable_scope
def arch_fusionnet_decoder_2d(img, feats=[None, None, None], last_dim=1, nl=tf.nn.tanh, nb_filters=32):
    assert img is not None
    with argscope([Conv2D], nl=INLReLU, kernel_shape=3, stride=2, padding='VALID'):
        with argscope([Deconv2D], nl=INLReLU, kernel_shape=3, stride=2, padding='SAME'):
            e3 = img
            e2, e1, e0 = feats
            d3 = residual_dec('d3',    e3, nb_filters*4)
            d2 = residual_dec('d2', d3+e2, nb_filters*2)
            d1 = residual_dec('d1', d2+e1, nb_filters*1)
            d0 = residual_dec('d0', d1+e0, nb_filters*1) 
            dp = tf.pad( d0, name='pad_o', mode='REFLECT', paddings=[[0,0], [3//2,3//2], [3//2,3//2], [0,0]])
            dd = Conv2D('convlast', dp, last_dim, kernel_shape=3, stride=1, padding='VALID', nl=nl, use_bias=True) 
            return dd, [d1, d2, d3]

@auto_reuse_variable_scope
def vgg19_encoder_flat(inputs, feats=[None, None, None, None, None], nb_filters=64, name='VGG19_Encoder'):

    with argscope([Conv2D], kernel_shape=3, nl=tf.nn.relu):
        feats_1, feats_2, feats_3 , feats_4 , feats_5 = feats

        conv1_1 = Conv2D('conv1_1', inputs,  nb_filters*1)
        conv1_1 = conv1_1+feats_1 if feats_1 is not None else conv1_1
        conv1_2 = Conv2D('conv1_2', conv1_1, nb_filters*1)
        pool1 = MaxPooling('pool1', conv1_2, 2) 
         # nb_filters*1
        conv2_1 = Conv2D('conv2_1', pool1,   nb_filters*2)
        conv2_1 = conv2_1+feats_2 if feats_2 is not None else conv2_1
        conv2_2 = Conv2D('conv2_2', conv2_1, nb_filters*2)
        pool2 = MaxPooling('pool2', conv2_2, 2)  # 32

        conv3_1 = Conv2D('conv3_1', pool2,   nb_filters*4)
        conv3_1 = conv3_1+feats_3 if feats_3 is not None else conv3_1
        conv3_2 = Conv2D('conv3_2', conv3_1, nb_filters*4)
        conv3_3 = Conv2D('conv3_3', conv3_2, nb_filters*4)
        conv3_4 = Conv2D('conv3_4', conv3_3, nb_filters*4)
        pool3 = MaxPooling('pool3', conv3_4, 2)  # 16

        conv4_1 = Conv2D('conv4_1', pool3,   nb_filters*8)
        conv4_1 = conv4_1+feats_4 if feats_4 is not None else conv4_1
        conv4_2 = Conv2D('conv4_2', conv4_1, nb_filters*8)
        conv4_3 = Conv2D('conv4_3', conv4_2, nb_filters*8)
        conv4_4 = Conv2D('conv4_4', conv4_3, nb_filters*8)
        pool4 = MaxPooling('pool4', conv4_4, 2)  # 16
        
        conv5_1 = Conv2D('conv5_1', pool4,   nb_filters*8)
        conv5_1 = conv5_1+feats_5 if feats_5 is not None else conv5_1
        conv5_2 = Conv2D('conv5_2', conv5_1, nb_filters*8)
        conv5_3 = Conv2D('conv5_3', conv5_2, nb_filters*8)
        conv5_4 = Conv2D('conv5_4', conv5_3, nb_filters*8)
        pool5 = MaxPooling('pool5', conv5_4, 2)  # 16

    
    return pool5, [conv5_1, conv4_1, conv3_1, conv2_1, conv1_1]

@auto_reuse_variable_scope
def vgg19_decoder_flat(inputs, feats=[None, None, None, None, None], nb_filters=64, name='VGG19_Decoder'):
    with argscope([Conv2D], kernel_shape=3, nl=tf.nn.leaky_relu):   
        with argscope([Deconv2D], kernel_shape=3, strides=(2,2), nl=tf.nn.leaky_relu):
            feats_5, feats_4, feats_3, feats_2, feats_1 = feats

            pool5 = Subpix2D('pool5',   inputs,  nb_filters*8)  # 16
            conv5_4 = Conv2D('conv5_4', pool5,   nb_filters*8)
            conv5_3 = Conv2D('conv5_3', conv5_4, nb_filters*8)
            conv5_2 = Conv2D('conv5_2', conv5_3, nb_filters*8)
            conv5_1 = Conv2D('conv5_1', conv5_2, nb_filters*8)
            conv5_1 = conv5_1+feats_5 if feats_5 is not None else conv5_1

            pool4 = Subpix2D('pool4',   conv5_1, nb_filters*8)  # 16
            conv4_4 = Conv2D('conv4_4', pool4,   nb_filters*8)
            conv4_3 = Conv2D('conv4_3', conv4_4, nb_filters*8)
            conv4_2 = Conv2D('conv4_2', conv4_3, nb_filters*8)
            conv4_1 = Conv2D('conv4_1', conv4_2, nb_filters*8)
            conv4_1 = conv4_1+feats_4 if feats_4 is not None else conv4_1
            pool3 = Subpix2D('pool3',   conv4_1, nb_filters*4)  # 16
            conv3_4 = Conv2D('conv3_4', pool3,   nb_filters*4)
            conv3_3 = Conv2D('conv3_3', conv3_4, nb_filters*4)
            conv3_2 = Conv2D('conv3_2', conv3_3, nb_filters*4)
            conv3_1 = Conv2D('conv3_1', conv3_2, nb_filters*4)
            conv3_1 = conv3_1+feats_3 if feats_3 is not None else conv3_1
            pool2 = Subpix2D('pool2',   conv3_1, nb_filters*2)  # 8
            conv2_2 = Conv2D('conv2_2', pool2,   nb_filters*2)
            conv2_1 = Conv2D('conv2_1', conv2_2, nb_filters*2)
            conv2_1 = conv2_1+feats_2 if feats_2 is not None else conv2_1
            pool1 = Subpix2D('pool1',   conv2_1, nb_filters*1)  # nb_filters*1
            conv1_2 = Conv2D('conv1_2', pool1,   nb_filters*1)
            conv1_1 = Conv2D('conv1_1', conv1_2, nb_filters*1)
            conv1_1 = conv1_1+feats_1 if feats_1 is not None else conv1_1

            conv1_0 = Conv2D('conv1_0', conv1_1, 1, nl=tf.nn.tanh)

            return conv1_0, [conv1_1, conv2_1, conv3_1, conv4_1, conv5_1]# List of feature maps














def time_seed ():
    seed = None
    while seed == None:
        cur_time = time.time ()
        seed = int ((cur_time - int (cur_time)) * 1000000)
    return seed

###############################################################################
class ImageDataFlow(RNGDataFlow):
    def __init__(self, imageDir, labelDir, noiseDir, size, dtype='float32', isTrain=False, isValid=False, isTest=False):
        self.dtype      = dtype
        self.imageDir   = imageDir
        self.labelDir   = labelDir
        self.noiseDir   = noiseDir
        self._size      = size
        self.isTrain    = isTrain
        self.isValid    = isValid

        
        images = natsorted (glob.glob(self.imageDir + '/*.tif'))
        labels = natsorted (glob.glob(self.labelDir + '/*.tif'))
        noises = natsorted (glob.glob(self.noiseDir + '/*.tif'))
        self.images = []
        self.labels = []
        self.noises = []
        self.data_seed = time_seed ()
        self.data_rand = np.random.RandomState(self.data_seed)
        self.rng = np.random.RandomState(999)
        for i in range (len (images)):
            image = images[i]
            self.images.append (skimage.io.imread (image))
        for i in range (len (labels)):
            label = labels[i]
            self.labels.append (skimage.io.imread (label))
        for i in range (len (noises)):
            noise = noises[i]
            self.noises.append (skimage.io.imread (noise))

        # print(self.imageDir)
        # print(self.labelDir)
        # print(self.noiseDir)
        
        # print(self.images)
        # print(self.labels)
        # print(self.noises)

        # print(self.images[0].shape)
        # print(self.labels[0].shape)
        # print(self.noises[0].shape)

        #self._size = 0
        #for i in range (len (self.images)):
        #   self._size += self.images[i].shape[0] * self.images[i].shape[1] * self.images[i].shape[2] \
        #           / (input_shape[0] * input_shape[1] * input_shape[2])

    def size(self):
        return self._size

    ###############################################################################
    # def AugmentPair(self, src_image, src_label, pipeline, seed=None, verbose=False):
    #   np.random.seed(seed) if seed else np.random.seed(2015)
        
    #   # Create the result
    #   aug_image = np.zeros_like(src_image)
    #   aug_label = np.zeros_like(src_label)
    #   # print(src_image.shape, src_label.shape, aug_image.shape, aug_label.shape) if verbose else ''
    #   for z in range(src_image.shape[0]):
    #       #Image and numpy has different matrix order
    #       pipeline.set_seed(seed)
    #       aug_image[z,...] = pipeline._execute_with_array(src_image[z,...]) 
    #       pipeline.set_seed(seed)
    #       aug_label[z,...] = pipeline._execute_with_array(src_label[z,...])        
    #   return aug_image, aug_label


    ###############################################################################
    def get_data(self):
        for k in range(self._size):
            #
            # Pick randomly a tuple of training instance
            #
            rand_image = self.data_rand.randint(0, len(self.images))
            rand_label = self.data_rand.randint(0, len(self.labels))
            rand_noise = self.data_rand.randint(0, len(self.noises))
            image = self.images[rand_image].copy ()
            label = self.labels[rand_label].copy ()
            noise = self.noises[rand_label].copy ()



            

            seed = time_seed () #self.rng.randint(0, 20152015)

            if self.isTrain:

                p = Augmentor.Pipeline()
                p.crop_by_size(probability=1, width=DIMX, height=DIMY, centre=False)
                p.rotate_random_90(probability=0.75, resample_filter=Image.NEAREST)
                p.rotate(probability=1, max_left_rotation=20, max_right_rotation=20, resample_filter=Image.NEAREST)
                #p.random_distortion(probability=1, grid_width=4, grid_height=4, magnitude=10)
                p.zoom_random(probability=0.5, percentage_area=0.8)
                p.flip_random(probability=0.75)

                image = p._execute_with_array(image) 
                label = p._execute_with_array(label) 
                noise = p._execute_with_array(noise) 
            else:
                p = Augmentor.Pipeline()
                p.crop_by_size(probability=1, width=DIMX, height=DIMY, centre=True)
                image = p._execute_with_array(image) 
                label = p._execute_with_array(label) 
                noise = p._execute_with_array(noise) 
                # pass
                

            #Expand dim to make single channel
            image = np.expand_dims(image, axis=-1)
            label = np.expand_dims(label, axis=-1)
            noise = np.expand_dims(noise, axis=-1)

            image = np.expand_dims(image, axis=0)
            label = np.expand_dims(label, axis=0)
            noise = np.expand_dims(noise, axis=0)

            yield [image.astype(np.float32), 
                   label.astype(np.float32), 
                   noise.astype(np.float32), 
                   ] 

    
###############################################################################
class ClipCallback(Callback):
    def _setup_graph(self):
        vars = tf.trainable_variables()
        ops = []
        for v in vars:
            n = v.op.name
            if not n.startswith('discrim/'):
                continue
            logger.info("Clip {}".format(n))
            ops.append(tf.assign(v, tf.clip_by_value(v, -0.01, 0.01)))
        self._op = tf.group(*ops, name='clip')

    def _trigger_step(self):
        self._op.run()



