import argparse
import numpy as np


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--load',        default='0', help='load the model.')
    parser.add_argument('--save',        default='0', help='save the model.')
    parser.add_argument('--vars',        default='0', help='variables to store: enc1 or dec0')

    args = parser.parse_args()

    oldFileName = args.load
    newFileName = args.save+'_'+args.vars

    # Read the model to the dictionary
    weights = np.load(oldFileName)
    dic = weights[()]

    # We choose only enc1 for the weight
    filtered_dic = {k:v for k,v in dic.iteritems() if args.vars in k}#if 'enc1' in k}
    renamed_dic  = filtered_dic.copy()
    for k, v in filtered_dic.iteritems():
        #k ['gen', 'enc1', 'conv1_1', 'W:0']
        tokens = k.split('/')[2:]  # ['conv1_1', 'W:0']
        kp = '/'.join(tokens) #'conv1_1/W:0'

        # Replace old key by new key
        if kp!=k:  
            renamed_dic[kp] = renamed_dic[k]
            del renamed_dic[k]

    # Save the new trimmed scope weight
    np.save(newFileName, renamed_dic)